import './ImageList.scss'
import {useEffect, useState} from "react";
import {NumberInOnePage} from "./consts";

export const ImageList = (Props) => {

    const {carousel, selectedIndex, updateBgMain, mouseEnter, mouseLeave} = Props

    const [isSelected, setIsSelected] = useState(
        Array.from({length: NumberInOnePage}, (_, index) =>
            index === 0
        )
    )
    //everytime fetch a group of pics to initialize the style
    useEffect(() => {
        setIsSelected(
            Array.from({length: NumberInOnePage}, (_, index) =>
                index === 0
            )
        )
    }, [carousel])

    //when selectedIndex from store.js changes,update isSelected Array
    useEffect(() => {
        setIsSelected(state => {
            let newState = [...state]
            newState.fill(false)
            newState[selectedIndex] = true
            return newState
        })
    }, [selectedIndex])

    return <>
        <div className="carousel">

            {carousel && carousel.map((item, index) => <div
                    key={index}
                    className={isSelected[index] ? 'imgContainer selected' : 'imgContainer'}

                    onClick={() => {
                        setIsSelected(state => {
                            let newState = [...state]
                            newState.fill(false)
                            newState[index] = true
                            return newState
                        })
                        updateBgMain(item)
                    }}

                    onMouseEnter={() => mouseEnter(index)}
                    onMouseLeave={mouseLeave}

                    style={{background: `url('${item.thumb}') no-repeat center center/cover fixed`}}
                />
            )
            }
        </div>
    </>
}