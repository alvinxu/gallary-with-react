import './App.scss';
import CityInput from "./CityInput";
import {ImageList} from "./ImageList";
import {PageNav} from "./PageNav";
import {ContextMenu} from "./ContextMenu";
import {PageJump} from "./PageJump";
import {useEffect, useRef, useState} from "react";
import {Logo} from "./Logo";
import {ToggleSwitch} from "./ToggleSwitch";

function App() {

    let app = useRef(null)

    const [input, setInput] = useState('')
    const [page, setPage] = useState(1)
    const [totalPages, setTotalPages] = useState(1)
    const [images, setImages] = useState([])
    const [des, setDes] = useState('')
    const [bgMainPage, setBgMainPage] = useState({background: 'black'})
    const [currentImage, setCurrentImage] = useState(null)
    const [showCMenu, setShowCMenu] = useState(false)
    const [xPage, setXPage] = useState(0)
    const [yPage, setYPage] = useState(0)
    const [useFullSize, setUseFullSize] = useState(false)


    //get images array from CityInput component
    const fetchImages = (input, images, pageAfterFetch, totalPages) => {
        if (images.length !== 0) {
            setImages(images)
            setBgMainPage({
                background:
                    useFullSize ?
                        `url('${images[0].full}') no-repeat center center/cover fixed` :
                        `url('${images[0].regular}') no-repeat center center/cover fixed`
            })
            setCurrentImage(images[0])
            setDes(images[0].des && images[0].des.charAt(0).toUpperCase() + images[0].des.slice(1))
            setInput(input)
            pageAfterFetch !== page && setPage(pageAfterFetch)
            setTotalPages(totalPages)
        }
    }

    //update main page background
    const updateBgMain = image => {

        setBgMainPage({
            background: useFullSize ?
                `url('${image.full}') no-repeat center center/cover fixed` :
                `url('${image.regular}') no-repeat center center/cover fixed`
        })
        setCurrentImage(image)
        setDes(!!image && image.des ? image.des.charAt(0).toUpperCase() + image.des.slice(1) : '')
    }

    // useEffect(() => console.log(bgMainPage), [bgMainPage])

    useEffect(() => {
        if (images.length !== 0) {
            console.log(`Images fetched! Background is the first pic!`)
        }
    }, [images])

    //when switchImg button triggered refresh the bg Img
    useEffect(()=>{
        currentImage && updateBgMain(currentImage)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    },[useFullSize])

    //when mouse move enter a carousel image
    const mouseEnter = index => {
        setBgMainPage({
            background: useFullSize ?
                `url('${images[index].full}') no-repeat center center/cover fixed` :
                `url('${images[index].regular}') no-repeat center center/cover fixed`
        })
        setDes(images[index].des && images[index].des.charAt(0).toUpperCase() + images[index].des.slice(1))
    }

    //when mouse move over a carousel image
    const mouseLeave = () => {
        setBgMainPage({
            background: useFullSize ?
                `url('${currentImage.full}') no-repeat center center/cover fixed` :
                `url('${currentImage.regular}') no-repeat center center/cover fixed`
        })
        setDes(currentImage.des && currentImage.des.charAt(0).toUpperCase() + currentImage.des.slice(1))
    }

    //handle pageNav button click event
    const handleLeftNavBtnClick = () => {
        images.length !== 0 && page <= 1 ? alert('Oops, This is already the first page...') :
            setPage(page => page - 1)
    }
    const handleRightNavBtnClick = () => {
        images.length !== 0 && page >= totalPages ? alert('Oops, This is the last page...') :
            setPage(page => page + 1)
    }

    //handle to change BgMain when click on carousel
    const changeImg = (e) => {
        if (showCMenu) {
            setShowCMenu(false)
        } else {
            if (e.target.className === 'App' && images.length !== 0) {
                if (images.indexOf(currentImage) >= images.length - 1)
                    handleRightNavBtnClick()
                else {
                    updateBgMain(images[images.indexOf(currentImage) + 1])
                }
            }
        }
    }

    //handle Jump from PageJump.js component
    const handleJump = (pageDest) => setPage(pageDest)

    //handle ContextMenu right click on App div
    const updateCmDisplay = (e) => {
        e.preventDefault()
        if (input) {
            console.log('this is cm')
            setShowCMenu(true)
            setXPage(e.pageX)
            setYPage(e.pageY)
        }
    }

    //handle functions for context menus
    const handleCmOpen = () => window.open(currentImage.full)
    const handleCmCopy = () => {
        fetch(`${currentImage.regular}`)
            .then(response => response.blob())
            .then(blob => convertToPNG(blob))
            .then(pngBlob => {
                const item = new ClipboardItem({'image/png': pngBlob})
                navigator.clipboard.write([item])
                    .then(() => alert('Succeed! copied to clipboard!'), error => alert(error))
            })
    }

    const convertToPNG = blob =>
        new Promise(resolve => {
            const img = new Image();
            img.src = URL.createObjectURL(blob);
            img.onload = () => {
                const canvas = Object.assign(document.createElement('canvas'), {
                    width: img.width,
                    height: img.height
                });
                canvas.getContext('2d').drawImage(img, 0, 0);
                canvas.toBlob(resolve, 'image/png');
                URL.revokeObjectURL(img.src)
            };
        });

    const handleCmSave = () => {
        fetch(`${currentImage.full}`)
            .then(response => response.blob())
            .then(blob => {
                let link = document.createElement('a')
                link.href = URL.createObjectURL(blob);
                link.download = 'image.jpg'
                link.click();
                URL.revokeObjectURL(link.href)
            })
    }

    return (
        <div
            className="App"
            ref={app}
            style={bgMainPage}
            onClick={changeImg}
            onContextMenu={updateCmDisplay}>

            <div className='desImage'>
                {des}
            </div>
            <Logo input={input}/>
            <CityInput
                submitImages={fetchImages}
                pageToJump={page}
            />
            <PageNav
                onClickLeft={handleLeftNavBtnClick}
                onClickRight={handleRightNavBtnClick}
                input={input}
            />
            <ImageList
                carousel={images}
                selectedIndex={images.indexOf(currentImage)}
                updateBgMain={updateBgMain}
                mouseEnter={mouseEnter}
                mouseLeave={mouseLeave}
            />
            <PageJump
                input={input}
                page={page}
                totalPages={totalPages}
                updatePageNumber={handleJump}
            />
            <ContextMenu
                leftPos={xPage}
                topPos={yPage}
                isShow={showCMenu}
                handleOpen={handleCmOpen}
                handleCopy={handleCmCopy}
                handleSave={handleCmSave}
            />

            {images.length > 0 && <ToggleSwitch
                switchImgDef={() => {
                    setUseFullSize(state => !state)
                }}
                parentRef={app}
            />}

        </div>
    );
}

export default App;
