import './PageJump.scss'
import {useState} from "react";

export const PageJump = ({input, page, totalPages, updatePageNumber}) => {

    const [destPage, setDestPage] = useState(1)

    //Jump to the destination page either with click or 'Enter' button
    const handleJump = () => updatePageNumber(destPage)
    const cbHandleJump = e => e.key === 'Enter' && updatePageNumber(destPage)

    return <>
        {input && <div className="pageNav">
            <span className="pageNumber">
                <span style={{
                    color: 'black',
                    borderRadius: '4px',
                    fontSize: '15px',
                    fontWeight: 'bold',
                    backgroundColor: 'rgba(255,165,0,0.5)',
                    width: 'fit-content',
                    padding: '0 3px',
                }}>{input}</span>
                <span style={{fontSize: '20px', color: 'cyan', fontWeight: 'bold'}}>{page}</span>
                /{totalPages}
            </span>
            <input
                className="pageDestNumber"
                type="text"
                onChange={e => setDestPage(parseInt(e.target.value))}
                onKeyUp={cbHandleJump}
            />
            <button onClick={handleJump}>
                <ion-icon title='Go' name="play"></ion-icon>
            </button>
        </div>}
    </>
}