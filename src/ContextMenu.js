import './ContextMenu.scss'

export const ContextMenu = ({leftPos, topPos, isShow,handleOpen,handleCopy,handleSave}) => {

    return <>
        <div
            className={isShow ? "menuContainer show" : 'menuContainer'}
            style={{left:`${leftPos}px`,top:`${topPos}px`}}

        >
            <ul className="menus">
                <li className="menu" id="openMenu" onClick={handleOpen}>
                    <ion-icon name="image"></ion-icon>
                    Open
                </li>
                <li className="menu" id="copyMenu" onClick={handleCopy}>
                    <ion-icon name="images"></ion-icon>
                    Copy image
                </li>
                <li className="menu" id="saveMenu" onClick={handleSave}>
                    <ion-icon name="save"></ion-icon>
                    Save as file
                </li>
            </ul>
        </div>

    </>
}