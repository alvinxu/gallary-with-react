import {useEffect, useState} from "react";
import {AccessKey, BasicUrl} from "./consts";
import axios from "axios";

import './CityInput.scss'

const CityInput = ({submitImages, pageToJump}) => {
    //Both in this component and App component store a 'input'
    //they are synchronized after fetch()
    const [input, setInput] = useState('')

    //temp variable for store value of input to save resource
    const [tempInput, setTempInput] = useState('')

    //keyUp to change input on 'Enter'
    const cbChangeInput = e => {
        let newInput = e.target.value.trim().toLowerCase()
        e.key === 'Enter' && !!newInput && setInput(newInput)
    }

    //btnSearch onClick
    const cbBtnSearch = () => {
        if (!!tempInput) {
            setInput(tempInput)
        }
    }

    //After input or page# changed, go to Fetch
    //useEffect can ensure to fetch() only when input or pages change
    useEffect(() => {
            if (input !== '') fetch(1)
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [input]
    )

    useEffect(() => {
            if (input !== '') fetch(pageToJump)
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [pageToJump]
    )

    const fetch = (page) => {

        //axios: 3rd party library
        //https://www.npmjs.com/package/axios

        // console.log(`Now, ${input} is fetching...`)
        axios.get(BasicUrl, {
            params: {
                query: input,
                orientation: 'landscape',
                page: page
            },
            headers: {
                authorization: `Client-ID ${AccessKey}`
            }
        })
            .then(response => {
                // console.log(response)
                let {data: {results, total_pages: totalPages}} = response
                let imageList = results.map((item, _) => ({
                    des: item.alt_description,
                    full: item.urls.full,
                    regular: item.urls.regular,
                    thumb: item.urls.thumb
                }))

                !!imageList && submitImages(input, imageList, page, totalPages)

            })
            .catch(err => console.log('catch error!', err))
    }


    return <>
        <div className={input ? "searchBar" : 'searchBar show'}>
            <input type="text"
                   className="inputCity"
                   onChange={e => setTempInput(e.target.value)}
                   onKeyUp={cbChangeInput}/>
            <button className="btnSearch" onClick={cbBtnSearch}>
                <ion-icon name="search"></ion-icon>
            </button>
        </div>
    </>

}

export default CityInput