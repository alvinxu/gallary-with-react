import './PageNav.scss'

export const PageNav = (Props) => {

    const {onClickLeft, onClickRight, input} = Props

    return <>
        <div className={input ? "nav" : 'nav hide'}>
            <button className="navBtn prev" onClick={onClickLeft}>
                <ion-icon name="chevron-back"></ion-icon>
            </button>
            <button className="navBtn next" onClick={onClickRight}>
                <ion-icon name="chevron-forward"></ion-icon>
            </button>
        </div>
    </>
}