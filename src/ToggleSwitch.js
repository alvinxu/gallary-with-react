import {Switch, View} from "react-native-web";
import {useEffect, useRef, useState} from "react";

export const ToggleSwitch = ({parentRef, switchImgDef}) => {

    const [isToggled, setIsToggled] = useState(false)
    const [isFullSize, setIsFullSize] = useState(false)

    const timerRef = useRef(null)

    //Auto play
    useEffect(() => {
        if (isToggled)
            timerRef.current = setInterval(() => parentRef.current.click(), 20000)
        return () => clearInterval(timerRef.current)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isToggled])

    //change Image Defination (HD <-> SD)
    useEffect(() => {
        return () => switchImgDef()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFullSize])

    return <>

        <View style={{
            width: '10vw',
            bottom: '0',
            left: '5vw',
            display: 'flex',
            flexDirection: 'row',
            gap: '10px',
            justifyContent: 'flex-start',
            position: 'absolute',
            whiteSpace: 'nowrap'
        }}>
            {/*<Text style={{color: 'cyan'}}>AUTOPLAY:</Text>*/}
            <button style={{
                width: '40px',
                background: isFullSize ? 'rgba(255, 165, 0, 0.5)' : 'rgba(0,0,100,0.2)',
                border: 'none',
                fontSize: '12px',
                fontWeight: 'bold',
                color: isFullSize ? 'cyan' : 'white',
                borderRadius: '5px',

            }} onClick={() => setIsFullSize(state => !state)}>
                {isFullSize ? 'H.D.' : 'S.D.'}
            </button>

            <Switch
                trackColor={{false: "#767577", true: "cyan"}}
                thumbColor={isToggled ? "orange" : "white"}
                ios_backgroundColor="#3e3e3e"
                onValueChange={() => setIsToggled(previousState => !previousState)}
                value={isToggled}
            />


        </View>

    </>
}